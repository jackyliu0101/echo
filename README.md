# README #

### What is Echo? ###

Echo is an android app written based on the visual memory game that can be found on https://humanbenchmark.com/tests/memory

Developed for SFU CMPT 275 Fall 2020

Group Members:

1. Jacky Liu 
	- Student ID: 301336351 
	- jfl6@sfu.ca
2. Ryan (Jae Hyun) Kim  
	- Student ID: 301339515 
	- jhk31@sfu.ca
3. Alfred Rodillo 
	- Student ID: 301347924
	- arodillo@fu.ca

### How do I get set up? ###

The app can be run on an android emulator in Android Studios.
Information on how to set up the emulator can be found here: https://developer.android.com/studio/run/emulator

### Basic code organization ###

* Java source code can be found under Echo/app/src/main/java/com/example/echo/
* Unit tests can be found under Echo/app/src/test/java/com/example/echo/
	- written using JUnit version 4.12
* UI tests can be found under Echo/app/src/androidTest/java/com/example/echo/
	- UI tests were written using Espresso

Information regarding Espresso can be found here: https://developer.android.com/training/testing/espresso

### More info ###
* Bitbucket repository: https://bitbucket.org/jackyliu0101/echo/src/master/