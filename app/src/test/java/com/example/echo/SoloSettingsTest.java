package com.example.echo;

import org.junit.Test;
import static org.junit.Assert.*;

public class SoloSettingsTest {
    GameSettings settings = SoloGameSettings.getInstance();

    @Test
    public void isTime_true() {
        settings.setIsTimed(true);
        assertTrue(settings.getIsTimed());
    }

    @Test
    public void isTime_false() {
        settings.setIsTimed(false);
        assertFalse(settings.getIsTimed());
    }

    @Test
    public void setTime_3min() {
        settings.setTime(3);
        assertEquals(3, settings.getTime());
    }

    @Test
    public void setTime_5min() {
        settings.setTime(5);
        assertEquals(5, settings.getTime());
    }

    @Test
    public void boardSize_6x6() {
        settings.setMaxBoardSize(6);
        assertEquals(6, settings.getMaxBoardSize());
    }

    @Test
    public void boardSize_7x7() {
        settings.setMaxBoardSize(7);
        assertEquals(7, settings.getMaxBoardSize());
    }

    @Test
    public void boardSize_8x8() {
        settings.setMaxBoardSize(8);
        assertEquals(8, settings.getMaxBoardSize());
    }

    @Test
    public void boardSize_infinite() {
        settings.setMaxBoardSize(0);
        assertEquals(0, settings.getMaxBoardSize());
    }

}
