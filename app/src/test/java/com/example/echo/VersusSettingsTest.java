package com.example.echo;

import org.junit.Test;
import static org.junit.Assert.*;

public class VersusSettingsTest {
    @Test
    public void isTime_isCorrect() {
        GameSettings settings = new VersusGameSettings();

        settings.setIsTimed(true);
        assertTrue("should always be true in versus mode", settings.getIsTimed());

        // isTimed should still be true
        settings.setIsTimed(false);
        assertTrue("should always be true in versus mode", settings.getIsTimed());
    }

    @Test
    public void setTime_3min() {
        GameSettings settings = new VersusGameSettings();
        settings.setTime(3);

        assertEquals(settings.getTime(), 3);
    }

    @Test
    public void setTime_5min() {
        GameSettings settings = new VersusGameSettings();
        settings.setTime(5);

        assertEquals(settings.getTime(), 5);
    }

    @Test
    public void boardSize_6x6() {
        GameSettings settings = new VersusGameSettings();
        settings.setMaxBoardSize(6);

        assertEquals(6, settings.getMaxBoardSize());
    }

    @Test
    public void boardSize_7x7() {
        GameSettings settings = new VersusGameSettings();
        settings.setMaxBoardSize(7);

        assertEquals(7, settings.getMaxBoardSize());
    }

    @Test
    public void boardSize_8x8() {
        GameSettings settings = new VersusGameSettings();
        settings.setMaxBoardSize(8);

        assertEquals(8, settings.getMaxBoardSize());
    }

    @Test
    public void boardSize_infinite() {
        GameSettings settings = new VersusGameSettings();
        settings.setMaxBoardSize(0);

        assertEquals(0, settings.getMaxBoardSize());
    }
}
