package com.example.echo;

import android.app.Activity;
import android.app.Instrumentation;

import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.*;

public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule =
            new ActivityTestRule<MainActivity>(MainActivity.class);

    private MainActivity mainActivity = null;

    @Before
    public void setUp() throws Exception {
        mainActivity = mainActivityActivityTestRule.getActivity();
    }

    /* Check if the New Game button successfully launches the correct activity.
    * */
    @Test
    public void testNewGame() {
        assertNotNull(mainActivity.findViewById(R.id.btnNewGame));
        Instrumentation.ActivityMonitor monitor = getInstrumentation().addMonitor(NewGameActivity.class.getName(), null, false);
        onView(withId(R.id.btnNewGame)).perform(click());
        Activity newGameActivity = monitor.waitForActivityWithTimeout(5000);
        assertNotNull(newGameActivity);
        newGameActivity.finish();
    }

    /* Check if the How To Play button successfully launches the correct activity.
    * */
    @Test
    public void testHowToPlay() {
        assertNotNull(mainActivity.findViewById(R.id.btnInstructions));
        Instrumentation.ActivityMonitor monitor = getInstrumentation().addMonitor(InstructionsActivity.class.getName(), null, false);
        onView(withId(R.id.btnInstructions)).perform(click());
        Activity instructionsActivity = monitor.waitForActivityWithTimeout(5000);
        assertNotNull(instructionsActivity);
        instructionsActivity.finish();
    }

    /* Check if the Preferences button successfully opens the associated dialog.
    * */
    @Test
    public void testPreferences() {
        assertNotNull(mainActivity.findViewById(R.id.btnPreferences));
        onView(withId(R.id.btnPreferences)).perform(click());
        onView(withId(R.id.textPreferences)).inRoot(isDialog()).check(matches(isDisplayed()));
    }

    @After
    public void tearDown() throws Exception {
        mainActivity = null;
    }
}