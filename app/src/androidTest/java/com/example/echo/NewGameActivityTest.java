package com.example.echo;

import android.app.Activity;
import android.app.Instrumentation;

import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;
import static androidx.test.espresso.matcher.ViewMatchers.isChecked;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isNotChecked;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.*;

public class NewGameActivityTest {

    @Rule
    public ActivityTestRule<NewGameActivity> newGameActivityTestRule =
            new ActivityTestRule<NewGameActivity>(NewGameActivity.class);

    private NewGameActivity newGameActivity = null;

    @Before
    public void setUp() throws Exception {
        newGameActivity = newGameActivityTestRule.getActivity();
    }

    /* Check that only one option may be selected at any given time.
    * */
    @Test
    public void testRadioButtons() {
        assertNotNull(newGameActivity.findViewById(R.id.radioBtnSolo));

        // Default button to be checked should be "New Solo Game"
        onView(withId(R.id.radioBtnSolo)).check(matches(isChecked()));

        onView(withId(R.id.radioBtnVersus)).perform(click());
        onView(withId(R.id.radioBtnVersus)).check(matches(isChecked()));
        onView(withId(R.id.radioBtnSolo)).check(matches(isNotChecked()));
        onView(withId(R.id.radioBtnJoin)).check(matches(isNotChecked()));

        onView(withId(R.id.radioBtnJoin)).perform(click());
        onView(withId(R.id.radioBtnJoin)).check(matches(isChecked()));
        onView(withId(R.id.radioBtnVersus)).check(matches(isNotChecked()));
        onView(withId(R.id.radioBtnSolo)).check(matches(isNotChecked()));

        onView(withId(R.id.radioBtnSolo)).perform(click());
        onView(withId(R.id.radioBtnSolo)).check(matches(isChecked()));
        onView(withId(R.id.radioBtnVersus)).check(matches(isNotChecked()));
        onView(withId(R.id.radioBtnJoin)).check(matches(isNotChecked()));
    }

    /* Check that selecting New Solo Game successfully launches the correct Settings screen.
    *  */
    @Test
    public void testNewSoloGame() {
        assertNotNull(newGameActivity.findViewById(R.id.btnNext));
        onView(withId(R.id.radioBtnSolo)).check(matches(isChecked()));
        Instrumentation.ActivityMonitor monitor = getInstrumentation().addMonitor(SoloSettingsActivity.class.getName(), null, false);
        onView(withId(R.id.btnNext)).perform(click());
        Activity soloSettingsActivity = monitor.waitForActivityWithTimeout(5000);
        assertNotNull(soloSettingsActivity);
        soloSettingsActivity.finish();
    }

    /* Check that selecting New Versus Game successfully launches the correct Settings screen.
    * */
    @Test
    public void testNewVersusGame() {
        assertNotNull(newGameActivity.findViewById(R.id.btnNext));
        onView(withId(R.id.radioBtnVersus)).check(matches(isNotChecked()));
        onView(withId(R.id.radioBtnVersus)).perform(click());
        onView(withId(R.id.radioBtnVersus)).check(matches(isChecked()));
        Instrumentation.ActivityMonitor monitor = getInstrumentation().addMonitor(VersusSettingsActivity.class.getName(), null, false);
        onView(withId(R.id.btnNext)).perform(click());
        Activity versusSettingsActivity = monitor.waitForActivityWithTimeout(5000);
        assertNotNull(versusSettingsActivity);
        versusSettingsActivity.finish();
    }

    /* Check that selecting Join Versus Game successfully opens the associated dialog allowing the
    user to enter the game code.
    * */
    @Test
    public void testJoinVersusGame() {
        assertNotNull(newGameActivity.findViewById(R.id.btnNext));
        onView(withId(R.id.radioBtnJoin)).check(matches(isNotChecked()));
        onView(withId(R.id.radioBtnJoin)).perform(click());
        onView(withId(R.id.radioBtnJoin)).check(matches(isChecked()));
        onView(withId(R.id.btnNext)).perform(click());
        onView(withId(R.id.editGameCode)).inRoot(isDialog()).check(matches(isDisplayed()));
    }

    /* Check that pressing the Back button successfully returns the user to the main menu screen.
    * */
    @Test
    public void testBackButton() {
        assertNotNull(newGameActivity.findViewById(R.id.btnBack));
        onView(withId(R.id.btnBack)).perform(click());
        assertTrue(newGameActivityTestRule.getActivity().isFinishing());
    }

    @After
    public void tearDown() throws Exception {
        newGameActivity = null;
    }
}