package com.example.echo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button startGameBtn = findViewById(R.id.btnNewGame);
        Button instructionsBtn = findViewById(R.id.btnInstructions);
        Button preferencesBtn = findViewById(R.id.btnPreferences);

        // start game Button
        startGameBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent startNewGame = new Intent(getApplicationContext(), NewGameActivity.class);
                startActivity(startNewGame);
            }
        });

        instructionsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent instructions = new Intent(getApplicationContext(), InstructionsActivity.class);
                startActivity(instructions);
            }
        });

        // preferences Button
        preferencesBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPreferencesDialog();
            }
        });
    }

    public void openPreferencesDialog() {
        PreferencesDialog preferencesDialog = new PreferencesDialog();
        preferencesDialog.show(getSupportFragmentManager(), "Preferences Dialogue");
    }
}