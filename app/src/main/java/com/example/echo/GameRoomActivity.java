package com.example.echo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class GameRoomActivity extends AppCompatActivity {
    TextView readyText;
    DatabaseReference readyRef;
    ValueEventListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_room);

        TextView gameCodeText = findViewById(R.id.textCode);
        readyText = findViewById(R.id.textReady);

        // displays the game code for the current room
        gameCodeText.setText(getIntent().getStringExtra("code"));
        addEventListener();
    }

    @Override
    protected void onDestroy() {
        readyRef.removeEventListener(listener);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        leaveRoom(null);
    }

    public void startGame(View view) {
        String gameCode = getIntent().getStringExtra("code");
        DatabaseReference roomRef = FirebaseDatabase.getInstance().getReferenceFromUrl(getIntent().getStringExtra("reference"));
        roomRef.child("hasStarted").setValue(true);

        Intent intent = new Intent(GameRoomActivity.this, VersusGameActivity.class);
        intent.putExtra("reference", roomRef.toString());
        intent.putExtra("code", gameCode);
        intent.putExtra("role", "host");
        startActivity(intent);
        finish();
    }

    // Leave button
    public void leaveRoom(View view) {
        final DatabaseReference roomRef = FirebaseDatabase.getInstance().getReferenceFromUrl(getIntent().getStringExtra("reference"));

        AlertDialog.Builder builder = new AlertDialog.Builder(GameRoomActivity.this);
        builder.setTitle("Leave game room");
        builder.setMessage("Are you sure you want to leave this game room?");

        // delete room from database and end activity
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                readyRef.removeEventListener(listener);

                roomRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        // if the guest player is in the room, then just remove the host
                        // if only the host player is in the room, then the entire room can be removed.
                        if (snapshot.hasChild("guest")) {
                            roomRef.child("host").removeValue();
                        } else {
                            roomRef.removeValue();
                        }
                        finish();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                    }
                });
            }
        });

        builder.setNegativeButton("No", null);
        builder.show();

    }

    // checks if player 2 (i.e. guest player) has entered the game room
    public void addEventListener(){
        final DatabaseReference roomRef = FirebaseDatabase.getInstance().getReferenceFromUrl(getIntent().getStringExtra("reference"));
        readyRef = roomRef.child("readyToStart");
        final Button startBtn = findViewById(R.id.btnStart);

        listener = readyRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                // checks the boolean readyToStart in the Firebase Database
                if (snapshot.getValue(boolean.class)) {
                    // if readyToStart == true, display to the screen that the game is ready to be started, and enable the start button
                    readyText.setText("Player2 is ready!");
                    startBtn.setEnabled(true);
                } else {
                    // readyToStart == false (i.e. when the guest has not yet joined the game room, or if they leave the game room)
                    readyText.setText("Waiting for Player 2");
                    startBtn.setEnabled(false);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}