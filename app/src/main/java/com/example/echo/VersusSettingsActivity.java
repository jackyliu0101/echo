package com.example.echo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Random;

public class VersusSettingsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_versus_settings);

        Button backBtn = findViewById(R.id.btnBack);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    // Responds to "create game" button;
    // writes all the settings to firebase, and creates a game room.
    public void createGame(View view) {
        GameSettings settings = new VersusGameSettings();
        RadioButton radioButton3min = findViewById(R.id.radioBtn3min);
        RadioButton radioButton5min = findViewById(R.id.radioBtn5min);
        RadioButton radioButton6x6 = findViewById(R.id.radioBtn6x6);
        RadioButton radioButton7x7 = findViewById(R.id.radioBtn7x7);
        RadioButton radioButton8x8 = findViewById(R.id.radioBtn8x8);
        RadioButton radioButtonInfinite = findViewById(R.id.radioBtnInfinite);

        String gameCode = gameCodeGenerator(6);
        Player hostPlayer = new Player(0);

        settings.setIsTimed(true);

        // time (i.e. game duration)
        if (radioButton3min.isChecked()) {
            settings.setTime(3);
        } else if (radioButton5min.isChecked()) {
            settings.setTime(5);
        }

        // max board size
        if (radioButtonInfinite.isChecked()) {
            settings.setMaxBoardSize(0);
        } else if (radioButton6x6.isChecked()) {
            settings.setMaxBoardSize(6);
        } else if (radioButton7x7.isChecked()) {
            settings.setMaxBoardSize(7);
        } else if (radioButton8x8.isChecked()) {
            settings.setMaxBoardSize(8);
        }

        // getting a reference to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference roomRef = database.getReference("Rooms");
        DatabaseReference newRoomRef = roomRef.push();

        // writing settings to database
        newRoomRef.child("gameCode").setValue(gameCode);
        newRoomRef.child("host").setValue(hostPlayer);
        newRoomRef.child("settings").setValue(settings);
        newRoomRef.child("readyToStart").setValue(false);
        newRoomRef.child("hasStarted").setValue(false);

        // create game room
        Intent intent = new Intent(getApplicationContext(), GameRoomActivity.class);
        intent.putExtra("reference", newRoomRef.toString());
        intent.putExtra("code", gameCode);
        startActivity(intent);
        finish();
    }

    // generates a random alphanumeric code for the game room
    public String gameCodeGenerator(int length) {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder code = new StringBuilder();

        Random rnd = new Random();

        for (int i = 0; i < length; i++) {
            int index = (int)(rnd.nextFloat()*chars.length());
            code.append(chars.charAt(index));
        }

        return code.toString();
    }
}