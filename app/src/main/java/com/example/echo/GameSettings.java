package com.example.echo;

public interface GameSettings {
    void setIsTimed(boolean isTimed);
    boolean getIsTimed();
    void setTime(int time);
    int getTime();
    void setMaxBoardSize(int maxSize);
    int getMaxBoardSize();
}
