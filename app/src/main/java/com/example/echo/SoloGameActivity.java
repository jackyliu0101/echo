package com.example.echo;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AlertDialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class SoloGameActivity extends AppCompatActivity {
    private final int MIN_BOARD_SIZE = 3;
    private int numCols;
    private int numTilesToSelect;
    private Boolean[] tileSet;
    private GridView gridView;
    private BoardAdapter boardAdapter;
    private int lives;
    private int score;
    private int numCorrectOnCurrentSize; // Score on the current board size
    private int maxBoardSize;
    private TextView myScoreText;
    private TextView myLivesLabel;
    private TextView myLivesText;
    private TextView countdownText;
    private int counterTime;

    private SoloGameSettings settings;

    private CountDownTimer timer = null;

    private void makeNewBoard(int numCols) {
        int numTiles = numCols*numCols;
        tileSet = new Boolean[numTiles];
        boardAdapter.setTileSet(tileSet);
        boardAdapter.setNumTilesToSelect(numTilesToSelect);
        gridView.setNumColumns(numCols);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solo_game);

        myScoreText = findViewById(R.id.textScore);
        myLivesLabel = findViewById(R.id.textLivesLabel);
        myLivesText = findViewById(R.id.textLives);
        countdownText = findViewById(R.id.textCountdownSolo);

        settings = SoloGameSettings.getInstance();

        score = 0;
        numCorrectOnCurrentSize = 0;
        myScoreText.setText(Integer.toString(score));
        maxBoardSize = settings.getMaxBoardSize();
        numCols = MIN_BOARD_SIZE;
        tileSet = new Boolean[numCols*numCols];
        Arrays.fill(tileSet, Boolean.FALSE);
        maxBoardSize = settings.getMaxBoardSize();
        numTilesToSelect = numCols;
        boardAdapter = new BoardAdapter(this, numTilesToSelect, tileSet);
        gridView = findViewById(R.id.gridview);

        gridView.setAdapter(boardAdapter);

        // when one of the tiles is clicked, the corresponding boolean in testTileSet toggles (i.e. true to false and vice versa)
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                if (boardAdapter.getBoardClickable()) {
                    boardAdapter.checkSelection(position);
                    if (!tileSet[position]) {
                        tileSet[position] = !tileSet[position];
                        boardAdapter.notifyDataSetChanged();
                    }

                    if (boardAdapter.levelIsFinished()) {
                        boardAdapter.setBoardClickable(false);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (boardAdapter.levelIsCorrect()) {
                                    score++;
                                    myScoreText.setText(Integer.toString(score));
                                    if ((numCols < maxBoardSize) || (maxBoardSize == 0)) {
                                        numCorrectOnCurrentSize++;
                                        if (numCorrectOnCurrentSize == (numCols + 2)) {
                                            numCorrectOnCurrentSize = 0;
                                            numCols++;
                                        }
                                    }
                                    if ((score % 2) == 0) {
                                        int numTiles = numCols * numCols;
                                        if (numTilesToSelect < (numTiles / 2)) {
                                            numTilesToSelect++;
                                        }
                                    }
                                    makeNewBoard(numCols);
                                } else if (!settings.getIsTimed()) {
                                    lives--;
                                    myLivesText.setText(Integer.toString(lives));
                                    if (lives == 0) {
                                        endCurrentGame();
                                    }
                                }
                                boardAdapter.startLevel();
                            }
                        }, 500);
                    }
                }
            }
        });

        final Handler countdownHandler = new Handler();

        gridView.setVisibility(View.INVISIBLE);
        counterTime = 3;
        countdownText.setText(Integer.toString(counterTime));
        final Runnable counter = new Runnable() {
            @Override
            public void run() {
                counterTime--;
                countdownText.setText(Integer.toString(counterTime));
                if (counterTime >= 1) {
                    countdownHandler.postDelayed(this, 1000);
                } else {
                    countdownText.setVisibility(View.GONE);
                    gridView.setVisibility(View.VISIBLE);
                    boardAdapter.startLevel();
                    if (settings.getIsTimed()) {
                        startTimer(settings.getTime());
                    } else {
                        lives = 3;
                        myLivesLabel.setText("Lives:");
                        myLivesText.setText(Integer.toString(lives));
                    }
                }
            }
        };
        countdownHandler.postDelayed(counter, 1000);

    }

    // frees the timer thread
    @Override
    protected void onDestroy() {
        cancelTimer();

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SoloGameActivity.this);
        builder.setTitle("Quit Game");
        builder.setMessage("Are you sure you want to quit the game?");

        builder.setPositiveButton("Quit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SoloGameActivity.super.onBackPressed();
            }
        });

        builder.setNegativeButton("No, take me back", null);
        builder.show();
    }

    // starts the timer, and shows it on the UI
    public void startTimer(int minutes) {
        timer = new CountDownTimer(minutes*60*1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                TextView timerText = findViewById(R.id.textTimer);
                long minutesRemaining = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                long secondsRemaining = (millisUntilFinished - (minutesRemaining*60*1000))/1000;

                String timeRemaining = String.format(Locale.CANADA, "%02d:%02d", minutesRemaining, secondsRemaining);

                timerText.setText(timeRemaining);
            }

            @Override
            public void onFinish() {
                TextView timerText = findViewById(R.id.textTimer);
                timerText.setText("finished");
                boardAdapter.setBoardClickable(false);
                endCurrentGame();
            }
        }.start();
    }

    // destroys the timer
    public void cancelTimer() {
        if (timer != null) {
            timer.cancel();
        }
    }

    // Upon finishing a game, user should be redirected to the game over screen
    public void endCurrentGame() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), GameOverActivity.class);
                intent.putExtra("score", score);
                startActivity(intent);
                finish();
            }
        }, 500);
    }
}