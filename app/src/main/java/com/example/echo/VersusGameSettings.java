package com.example.echo;

public class VersusGameSettings implements GameSettings {
    private boolean isTimed;
    private int time;
    private int maxBoardSize;

    @Override
    public void setIsTimed(boolean isTimed) {
        this.isTimed = true;
    }

    @Override
    public boolean getIsTimed() {
        return isTimed;
    }

    @Override
    public void setTime(int time) {
        this.time = time;
    }

    @Override
    public int getTime() {
        return time;
    }

    @Override
    public void setMaxBoardSize(int maxSize) {
        this.maxBoardSize = maxSize;
    }

    @Override
    public int getMaxBoardSize() {
        return maxBoardSize;
    }
}
