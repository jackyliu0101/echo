package com.example.echo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class ThemeAdapter extends BaseAdapter {
    private final Context mContext;
    private final int[] coloursList;
    private int colourSelected;

    public ThemeAdapter(Context context, int[] colours, int selected) {
        this.mContext = context;
        this.coloursList = colours;
        this.colourSelected = selected;
    }

    @Override
    public int getCount() {
        return coloursList.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            final LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.board_layout, null);
        }

        final ImageView imageView = convertView.findViewById(R.id.tile);

        if (position == colourSelected) {
            imageView.setBackgroundColor(coloursList[position]);
            imageView.setImageResource(android.R.drawable.star_on);
        } else {
            imageView.setBackgroundColor(coloursList[position]);
            imageView.setImageResource(android.R.color.transparent);
        }

        return convertView;
    }

    public void refresh (int index) {
        this.colourSelected = index;
        notifyDataSetChanged();
    }
}
