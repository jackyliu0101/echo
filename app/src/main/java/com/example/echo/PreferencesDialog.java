package com.example.echo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.preference.PreferenceManager;

public class PreferencesDialog extends AppCompatDialogFragment {
    private int selectedIndex;
    private ThemeAdapter themeAdapter;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.preferences_layout, null);

        // getting colors for board theme from resource file
        int[] colours = getContext().getResources().getIntArray(R.array.boardThemes);

        // getting shared preferences
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        selectedIndex = sharedPreferences.getInt("prefBoardColour", 0);

        GridView gridView = view.findViewById(R.id.grid);
        themeAdapter = new ThemeAdapter(getContext(), colours, selectedIndex);
        gridView.setAdapter(themeAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedIndex = position;
                themeAdapter.refresh(selectedIndex);

                editor.putInt("prefBoardColour", selectedIndex);
                editor.apply();
            }
        });

        // closes the dialog when user presses "Ok"
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setView(view);
        AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        return dialog;
    }
}
