package com.example.echo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.renderscript.Sampler;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class WaitingRoomActivity extends AppCompatActivity {
    DatabaseReference readyRef;
    DatabaseReference roomRef;
    ValueEventListener readyListener;
    ValueEventListener roomListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_room);

        roomRef = FirebaseDatabase.getInstance().getReferenceFromUrl(getIntent().getStringExtra("reference"));
        roomRef.child("readyToStart").setValue(true);
        addEventListener();
    }

    // destroy the firebase value event listener when activity is destroyed
    @Override
    protected void onDestroy() {
        if (readyListener != null) {
            readyRef.removeEventListener(readyListener);
        }

        if (roomListener != null) {
            roomRef.removeEventListener(roomListener);
        }

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        leaveRoom(null);
    }

    // Leave button
    public void leaveRoom(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(WaitingRoomActivity.this);
        builder.setTitle("Leave game room");
        builder.setMessage("Are you sure you want to leave this game room?");

        // delete guest from room reference in database
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                roomRef.child("guest").removeValue();
                roomRef.child("readyToStart").setValue(false);
                finish();
            }
        });

        builder.setNegativeButton("No", null);
        builder.show();
    }

    public void addEventListener() {
        readyRef = roomRef.child("hasStarted");

        // checks if host has left the game room
        roomListener = roomRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (!snapshot.hasChild("host")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(WaitingRoomActivity.this);
                    builder.setTitle("Oh no!");
                    builder.setMessage("The host has left the game!");

                    // deletes the room from the database and leaves waiting room
                    builder.setPositiveButton("Leave game room", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            readyRef.removeEventListener(readyListener);
                            roomRef.removeEventListener(roomListener);
                            roomRef.removeValue();
                            finish();
                        }
                    });
                    builder.show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });

        // checks if host has started the game
        readyListener = readyRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                // checks the boolean hasStarted in the Firebase database
                if (snapshot.getValue(boolean.class)) {
                    Intent intent = new Intent(WaitingRoomActivity.this, VersusGameActivity.class);
                    intent.putExtra("reference", roomRef.toString());
                    intent.putExtra("role", "guest");
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}