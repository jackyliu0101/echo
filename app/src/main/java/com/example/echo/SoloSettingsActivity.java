package com.example.echo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

public class SoloSettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solo_settings);

        Button startBtn = findViewById(R.id.btnStartGameSolo);
        Button backBtn = findViewById(R.id.btnBackSolo);

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSoloGame();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void startSoloGame() {
        SoloGameSettings settings = SoloGameSettings.getInstance();
        RadioButton radioBtn3min = findViewById(R.id.radioBtn3minSolo);
        RadioButton radioBtn5min = findViewById(R.id.radioBtn5minSolo);
        RadioButton radioBtnNoTimeLimit = findViewById(R.id.radioBtnNoTimeLimit);
        RadioButton radioBtn6x6 = findViewById(R.id.radioBtn6x6Solo);
        RadioButton radioBtn7x7 = findViewById(R.id.radioBtn7x7Solo);
        RadioButton radioBtn8x8 = findViewById(R.id.radioBtn8x8Solo);
        RadioButton radioBtnInfBoard = findViewById(R.id.radioBtnInfiniteSolo);

        if (radioBtnNoTimeLimit.isChecked()) {
            settings.setIsTimed(false);
        } else {
            settings.setIsTimed(true);
        }

        if (radioBtn3min.isChecked()) {
            settings.setTime(3);
        } else if (radioBtn5min.isChecked()) {
            settings.setTime(5);
        }

        if (radioBtn6x6.isChecked()) {
            settings.setMaxBoardSize(6);
        } else if (radioBtn7x7.isChecked()) {
            settings.setMaxBoardSize(7);
        } else if (radioBtn8x8.isChecked()) {
            settings.setMaxBoardSize(8);
        } else if (radioBtnInfBoard.isChecked()) {
            settings.setMaxBoardSize(0);
        }

        Intent intent = new Intent(getApplicationContext(), SoloGameActivity.class);
        startActivity(intent);
        finish();
    }
}