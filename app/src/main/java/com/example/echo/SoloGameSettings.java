package com.example.echo;

public class SoloGameSettings implements GameSettings {
    private boolean isTimed;
    private int time;
    private int maxBoardSize;
    private static SoloGameSettings instance;

    // Singleton implementation
    private SoloGameSettings() {
        // Private to prevent external instantiation
    }

    public static SoloGameSettings getInstance() {
        if (instance == null) {
            instance = new SoloGameSettings();
        }
        return instance;
    }

    public void setIsTimed(boolean isTimed) {
        this.isTimed = isTimed;
    }
    public boolean getIsTimed() {
        return isTimed;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getTime(){
        return time;
    }

    public void setMaxBoardSize(int maxSize) {
        this.maxBoardSize = maxSize;
    }
    public int getMaxBoardSize() {
        return maxBoardSize;
    }
}
