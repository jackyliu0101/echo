package com.example.echo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.Arrays;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class VersusGameActivity extends AppCompatActivity {
    private final int MIN_BOARD_SIZE = 3;
    private int numCols;
    private int numTilesToSelect;
    private Boolean[] tileSet;
    private GridView gridView;
    private BoardAdapter boardAdapter;
    private int score;
    private int numCorrectOnCurrentSize; // Score on the current board size
    private int maxBoardSize;
    private TextView countdownText;
    private int counterTime;

    TextView myScoreText;
    TextView opponentScoreText;
    CountDownTimer timer = null;
    ValueEventListener listener;

    DatabaseReference roomRef;
    DatabaseReference opponentRef;
    DatabaseReference myPlayerRef;

    GameSettings settings = new VersusGameSettings();
    String myRole;
    String gameCode;

    private void makeNewBoard(int numCols) {
        int numTiles = numCols*numCols;
        tileSet = new Boolean[numTiles];
        boardAdapter.setTileSet(tileSet);
        boardAdapter.setNumTilesToSelect(numTilesToSelect);
        gridView.setNumColumns(numCols);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_versus_game);

        myRole = getIntent().getStringExtra("role");

        if (getIntent().hasExtra("code")) {
            gameCode = getIntent().getStringExtra("code");
        }

        // get text elements for the scores
        myScoreText = findViewById(R.id.textScore);
        opponentScoreText = findViewById(R.id.textOpponentScore);
        countdownText = findViewById(R.id.textCountdownVersus);

        // getting database references
        roomRef = FirebaseDatabase.getInstance().getReferenceFromUrl(getIntent().getStringExtra("reference"));

        if (myRole.equals("host")) {
            myPlayerRef = roomRef.child("host");
            opponentRef = roomRef.child("guest");
        } else {
            myPlayerRef = roomRef.child("guest");
            opponentRef = roomRef.child("host");
        }

        // initializing game variables
        score = 0;
        numCorrectOnCurrentSize = 0;
        numCols = MIN_BOARD_SIZE;
        numTilesToSelect = numCols;

        // Preparing the board
        tileSet = new Boolean[numCols*numCols];
        Arrays.fill(tileSet, Boolean.FALSE);
        boardAdapter = new BoardAdapter(this, numTilesToSelect, tileSet);

        gridView = findViewById(R.id.gridVersusBoard);
        gridView.setAdapter(boardAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                // Restrict when the board is allowed to respond to clicks (ex. time ran out, level not generated, etc.)
                if (boardAdapter.getBoardClickable()) {
                    boardAdapter.checkSelection(position);
                    if (!tileSet[position]) {
                        tileSet[position] = !tileSet[position];
                        boardAdapter.notifyDataSetChanged();
                    }

                    if (boardAdapter.levelIsFinished()) {
                        boardAdapter.setBoardClickable(false);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (boardAdapter.levelIsCorrect()) {
                                    updateScore();
                                    if ((numCols < maxBoardSize) || (maxBoardSize == 0)) {
                                        numCorrectOnCurrentSize++;
                                        if (numCorrectOnCurrentSize == (numCols + 2)) {
                                            numCorrectOnCurrentSize = 0;
                                            numCols++;
                                        }
                                    }
                                    if ((score % 2) == 0) {
                                        int numTiles = numCols * numCols;
                                        if (numTilesToSelect < (numTiles / 2)) {
                                            numTilesToSelect++;
                                        }
                                    }
                                    makeNewBoard(numCols);
                                }
                                boardAdapter.startLevel();
                            }
                        }, 500);
                    }
                }
            }
        });

        addGameEventListener();

        final Handler countdownHandler = new Handler();

        gridView.setVisibility(View.INVISIBLE);
        counterTime = 3;
        countdownText.setText(Integer.toString(counterTime));
        final Runnable counter = new Runnable() {
            @Override
            public void run() {
                counterTime--;
                countdownText.setText(Integer.toString(counterTime));
                if (counterTime >= 1) {
                    countdownHandler.postDelayed(this, 1000);
                } else {
                    countdownText.setVisibility(View.GONE);
                    gridView.setVisibility(View.VISIBLE);
                    boardAdapter.startLevel();
                    // get settings from database
                    getSettings(new SettingsCallback() {
                        @Override
                        public void callback() {
                            startTimer(settings.getTime());
                            maxBoardSize = settings.getMaxBoardSize();
                        }
                    });
                }
            }
        };
        countdownHandler.postDelayed(counter, 1000);
    }

    @Override
    protected void onResume() {
        myPlayerRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                score = snapshot.child("score").getValue(Integer.class);
                myScoreText.setText(Integer.toString(score));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });

        super.onResume();
    }

    // frees the timer thread and removes firebase value event listener when the activity is destroyed
    @Override
    protected void onDestroy() {
        cancelTimer();
        opponentRef.removeEventListener(listener);

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(getApplicationContext(), "Cannot leave game while it is ongoing.", Toast.LENGTH_LONG).show();
    }

    // starts the timer, and shows it on the UI
    public void startTimer(int minutes) {
        timer = new CountDownTimer(minutes*60*1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                TextView timerText = findViewById(R.id.textTimer);
                long minutesRemaining = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                long secondsRemaining = (millisUntilFinished - (minutesRemaining*60*1000))/1000;

                String timeRemaining = String.format(Locale.CANADA, "%02d:%02d", minutesRemaining, secondsRemaining);

                timerText.setText(timeRemaining);
            }

            @Override
            public void onFinish() {
                boardAdapter.setBoardClickable(false);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(getApplicationContext(), GameOverActivity.class);
                        intent.putExtra("role", myRole);
                        intent.putExtra("reference", roomRef.toString());

                        if (getIntent().hasExtra("code")) {
                            intent.putExtra("code", gameCode);
                        }

                        startActivity(intent);
                        finish();
                    }
                }, 500);

            }
        }.start();
    }

    // destroys the timer
    public void cancelTimer() {
        if (timer != null) {
            timer.cancel();
        }
    }

    public void getSettings(final SettingsCallback settingsCallback) {
        roomRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                settings.setTime(snapshot.child("settings").child("time").getValue(Integer.class));
                settings.setMaxBoardSize(snapshot.child("settings").child("maxBoardSize").getValue(Integer.class));
                settingsCallback.callback();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    // increases the score of the device's player
    public void updateScore() {
        myPlayerRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                score++;
                myPlayerRef.child("score").setValue(score);
                myScoreText.setText(Integer.toString(score));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    // event listener to the opponent's score
    // will update the UI when the opponent's score increases 
    private void addGameEventListener() {
        listener = opponentRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                int opponentScore = snapshot.child("score").getValue(Integer.class);
                opponentScoreText.setText(Integer.toString(opponentScore));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}