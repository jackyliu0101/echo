package com.example.echo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class GameCodeDialog extends AppCompatDialogFragment {
    private EditText code;
    private boolean gameCodeFound;
    private DatabaseReference roomRef;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.enter_game_code_layout, null);

        builder.setView(view);
        builder.setTitle("Enter Game Code");

        Button cancelBtn = view.findViewById(R.id.btnCancel);
        Button okBtn = view.findViewById(R.id.btnOk);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context currentContext = getContext();

                if (code.getText().toString().equals("")) {
                    Toast.makeText(currentContext, "Please enter a game code", Toast.LENGTH_SHORT).show();
                    return;
                }

                // search the database for a matching game code
                searchMatchingCode(new GameCodeCallback() {
                    // callback function after searchMatchingCode finishes
                    @Override
                    public void callback(boolean isFound) {
                        if (isFound) {
                            Intent intent = new Intent(currentContext, WaitingRoomActivity.class);
                            intent.putExtra("reference", roomRef.toString());
                            startActivity(intent);
                            getActivity().finish();
                        } else {
                            Toast.makeText(currentContext, "No game found for " + code.getText().toString() + " Try again.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        code = view.findViewById(R.id.editGameCode);

        return builder.create();
    }

    // searches the database for corresponding game code that the user entered
    public void searchMatchingCode(final GameCodeCallback gameCodeCallback) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final Player guestPlayer = new Player(0);
        gameCodeFound = false;

        // iterates through all the rooms in the database and looks for a matching code
        database.getReference().child("Rooms").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    if (code.getText().toString().equals(dataSnapshot.child("gameCode").getValue())) {
                        roomRef = dataSnapshot.getRef();
                        roomRef.child("guest").setValue(guestPlayer);

                        gameCodeFound = true;
                        break;
                    }
                }
                gameCodeCallback.callback(gameCodeFound);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}
