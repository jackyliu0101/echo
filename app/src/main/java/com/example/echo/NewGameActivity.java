package com.example.echo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

public class NewGameActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_game);

        Button nextBtn = findViewById(R.id.btnNext);
        Button backBtn = findViewById(R.id.btnBack);
        final RadioButton soloBtn = findViewById(R.id.radioBtnSolo);
        final RadioButton versusBtn = findViewById(R.id.radioBtnVersus);

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (soloBtn.isChecked()) {
                    Intent intent = new Intent(getApplicationContext(), SoloSettingsActivity.class);
                    startActivity(intent);
                } else if (versusBtn.isChecked()){
                    Intent intent = new Intent(getApplicationContext(), VersusSettingsActivity.class);
                    startActivity(intent);
                } else {
                    openGameCodeDialogue();
                }
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void openGameCodeDialogue() {
        GameCodeDialog gameCodeDialog = new GameCodeDialog();
        gameCodeDialog.show(getSupportFragmentManager(), "Game Code Dialogue");
    }
}