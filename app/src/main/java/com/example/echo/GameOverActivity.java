package com.example.echo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class GameOverActivity extends AppCompatActivity {
    DatabaseReference roomRef;
    DatabaseReference hasStartedRef;
    ValueEventListener roomListener;
    ValueEventListener hasStartedListener;
    boolean playAgain = false;

    TextView messageText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);

        messageText = findViewById(R.id.textMessage);

        if (getIntent().hasExtra("reference")) {
            // versus mode
            roomRef = FirebaseDatabase.getInstance().getReferenceFromUrl(getIntent().getStringExtra("reference"));
            roomRef.child("hasStarted").setValue(false);
            roomRef.child("readyToStart").setValue(false);

            showGameResult();

            if (getIntent().getStringExtra("role").equals("guest")) {
                checkHostListener();
            }
        } else {
            // solo mode
            int score = getIntent().getIntExtra("score", -1);
            String scoreMessage = "Score: " + score;
            messageText.setText(scoreMessage);
        }
    }

    @Override
    protected void onDestroy() {
        if (roomListener != null) {
            roomRef.removeEventListener(roomListener);
        }

        if (hasStartedListener != null) {
            hasStartedRef.removeEventListener(hasStartedListener);
        }

        super.onDestroy();
    }

    public void returnToMainMenu(View view) {
        // if versus mode, then we have to destroy the game room in the database before exiting
        if (getIntent().hasExtra("reference")) {
            final String myRole = getIntent().getStringExtra("role");

            roomRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.hasChild("guest") && snapshot.hasChild("host")) {
                        roomRef.child("readyToStart").setValue(false);
                        roomRef.child(myRole).removeValue();
                    } else {
                        roomRef.removeValue();
                    }

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                }
            });
        } else {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    public void playAgain(View view) {
        // if versus mode
        if (getIntent().hasExtra("reference")) {
            final String myRole = getIntent().getStringExtra("role");

            if (myRole.equals("host")) {
                // host presses "play again"
                roomRef.child("host").child("score").setValue(0);
                Intent intent = new Intent(getApplicationContext(), GameRoomActivity.class);
                intent.putExtra("reference", roomRef.toString());
                intent.putExtra("role", "host");

                if (getIntent().hasExtra("code")) {
                    String gameCode = getIntent().getStringExtra("code");
                    intent.putExtra("code", gameCode);
                }

                startActivity(intent);
                finish();
            } else {
                // guest presses "play again"
                ProgressBar progressBar = findViewById(R.id.progress_circular);
                Button playAgainBtn = findViewById(R.id.btnPlayAgain);
                playAgain = !playAgain;

                if (playAgain) {
                    playAgainBtn.setText("Cancel");
                    messageText.setText("Waiting for host to start game.");
                    messageText.setVisibility(view.VISIBLE);
                    progressBar.setVisibility(view.VISIBLE);

                    roomRef.child("guest").child("score").setValue(0);
                    roomRef.child("readyToStart").setValue(playAgain);
                    addReadyEventListener();
                } else {
                    playAgainBtn.setText("Play again");
                    progressBar.setVisibility(view.GONE);
                    messageText.setVisibility(view.GONE);
                    roomRef.child("readyToStart").setValue(playAgain);
                }
            }
        } else {
            // solo mode
            Intent intent = new Intent(getApplicationContext(), SoloGameActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void checkHostListener() {
        // checks if host has left the game room
        roomListener = roomRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (!snapshot.hasChild("host")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(GameOverActivity.this);
                    builder.setTitle("Oh no!");
                    builder.setMessage("The host has left the game!");

                    // deletes the room from the database and leaves waiting room
                    builder.setPositiveButton("Leave game room", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            roomRef.removeEventListener(roomListener);
                            if (hasStartedListener != null) {
                                hasStartedRef.removeEventListener(hasStartedListener);
                            }
                            roomRef.removeValue();

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    });
                    builder.show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    public void addReadyEventListener() {
        if (hasStartedRef != null) {
            return;
        }

        hasStartedRef = roomRef.child("hasStarted");

        // checks if host has started the game
        hasStartedListener = hasStartedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                // checks the boolean hasStarted in the Firebase database
                if (snapshot.getValue(boolean.class)) {
                    Intent intent = new Intent(getApplicationContext(), VersusGameActivity.class);
                    intent.putExtra("reference", roomRef.toString());
                    intent.putExtra("role", "guest");
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    public void showGameResult() {
        final String myRole = getIntent().getStringExtra("role");

        roomRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                int myScore;
                int opponentScore;
                TextView gameOverText = findViewById(R.id.textGameOver);

                if (myRole.equals("host")) {
                    myScore = snapshot.child("host").child("score").getValue(Integer.class);
                    opponentScore = snapshot.child("guest").child("score").getValue(Integer.class);
                } else {
                    myScore = snapshot.child("guest").child("score").getValue(Integer.class);
                    opponentScore = snapshot.child("host").child("score").getValue(Integer.class);
                }

                if (myScore > opponentScore) {
                    gameOverText.setText("You win!");
                } else if (opponentScore > myScore){
                    gameOverText.setText("You lose");
                } else {
                    gameOverText.setText("Tie!");
                }

                messageText.setSingleLine(false);
                messageText.setText("Your score: " + myScore + "\nOpponent: " + opponentScore);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
