package com.example.echo;

public interface GameCodeCallback {
    void callback(boolean isFound);
}
