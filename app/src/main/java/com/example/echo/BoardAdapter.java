package com.example.echo;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.view.View;
import android.widget.ImageView;

import androidx.preference.PreferenceManager;

import com.google.android.gms.common.util.ArrayUtils;

import java.util.Arrays;
import java.util.Random;


public class BoardAdapter extends BaseAdapter {
    private Context mContext;
    private int[] solution;
    private int numTilesToSelect;
    private int numTiles;
    private Boolean[] tileSet;
    private Random rand;
    private int correctSelected;
    private int wrongSelected;
    private boolean levelComplete;
    private boolean levelFailed;
    private boolean boardClickable;

    private final int MAX_SELECTIONS = 3;

    public BoardAdapter(Context context, int numTilesToSelect, Boolean[] tileSet) {
        this.mContext = context;
        this.numTilesToSelect = numTilesToSelect;
        this.numTiles = tileSet.length;
        this.tileSet = tileSet;
        this.rand = new Random();
        this.solution = new int[numTilesToSelect];
        this.correctSelected = 0;
        this.wrongSelected = 0;
        this.levelComplete = false;
        this.levelFailed = false;
        Arrays.fill(solution, -1);
    }

    public void setTileSet(Boolean[] tileSet) {
        this.tileSet = tileSet;
        this.numTiles = tileSet.length;
    }

    public void setNumTilesToSelect(int num) {
        this.numTilesToSelect = num;
        this.solution = new int[num];
    }

    public void setBoardClickable(boolean clickable) {
        this.boardClickable = clickable;
    }

    public boolean getBoardClickable() {
        return boardClickable;
    }

    // Randomly select which tiles are "correct"
    // The solution array will contain the indices
    public void selectRandomTiles() {
        int i = 0;
        while (i < numTilesToSelect) {
            int n = rand.nextInt(numTiles);
            if (ArrayUtils.contains(solution, n)) {
                continue;
            } else {
                solution[i] = n;
                tileSet[n] = !tileSet[n];
                i++;
            }
        }
    }

    // Check if the tile the user selected is "correct", and generate a new board upon finding all hidden tiles
    public void checkSelection(int position) {

        if (ArrayUtils.contains(solution, position)) {
            // If tile is already selected, don't count it again as a correct selection
            if (tileSet[position] && (correctSelected < numTilesToSelect)) {
                return;
            }
            correctSelected++;
        } else {
            // If tile had already been selected, don't count it again as an error
            if (tileSet[position]) {
                return;
            }
            wrongSelected++;
        }

        // Set variable if level is completed
        if (correctSelected == numTilesToSelect) {
            levelComplete = true;
        } else if (wrongSelected == MAX_SELECTIONS) {
            levelFailed = true;
        }
    }

    public void previewTiles() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < numTilesToSelect; i++) {
                    int n = solution[i];
                    tileSet[n] = !tileSet[n];
                }
                notifyDataSetChanged();
                setBoardClickable(true);
            }
        }, 1000);

    }

    public void startLevel() {
        //setBoardClickable(false);
        Arrays.fill(tileSet, Boolean.FALSE);
        Arrays.fill(solution, -1);
        notifyDataSetChanged();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                selectRandomTiles();
                notifyDataSetChanged();
                previewTiles();
            }
        }, 1000);
    }

    public boolean levelIsFinished() {
        if (levelComplete || levelFailed) {
            correctSelected = 0;
            wrongSelected = 0;
            if (levelFailed) {
                levelFailed = false;
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean levelIsCorrect() {
        if (levelComplete) {
            levelComplete = false;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getCount() {
        return numTiles;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            final LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.board_layout, null);
        }

        // getting colors for board theme from resource file
        int[] colours = mContext.getResources().getIntArray(R.array.boardThemes);
        int[] coloursDark = mContext.getResources().getIntArray(R.array.boardThemesDark);
        int[] coloursComp = mContext.getResources().getIntArray(R.array.boardThemesComp);

        // getting shared preferences
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        int prefColourIndex = sharedPreferences.getInt("prefBoardColour", 0);

        final ImageView imageView = convertView.findViewById(R.id.tile);

        if (tileSet[position]) {
            if (ArrayUtils.contains(solution, position)) {
                imageView.setBackgroundColor(coloursComp[prefColourIndex]);
            } else {
                imageView.setBackgroundColor(coloursDark[prefColourIndex]);
            }
        } else {
            imageView.setBackgroundColor(colours[prefColourIndex]);
        }

        return convertView;

    }
}
